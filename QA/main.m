//
//  main.m
//  QA
//
//  Created by Jérémie FOURBIL on 18/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QAAppDelegate class]));
    }
}
