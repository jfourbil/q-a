//
//  QAAppDelegate.h
//  QA
//
//  Created by Jérémie FOURBIL on 18/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QAViewController;

@interface QAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) QAViewController *viewController;

@end
